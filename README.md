A pet (pun indented) project to discover Cucumber.  
This will test the well-known JPetStore.

# Geckodriver  
Install Geckodriver (https://github.com/mozilla/geckodriver) so it is accessible with your $PATH

You can also define it from the maven command line  
`/h/Documents/tools/maven/3.8.4/bin/mvn test -Dwebdriver.gecko.driver=/c/Users/lmazure/bin/geckodriver.exe`

Or you can define in in the `pom.xml` file.

# SUT = JPetStore
to start the SUT:  
`docker run -d -p 8080:8080 --name jpetstore jloisel/jpetstore6`  
then it will be available in http://localhost:8080/actions/Catalog.action

