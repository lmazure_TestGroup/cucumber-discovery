package jpetstoretest;

import java.sql.Timestamp;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import jpetstoretest.pageobjects.CartPageObject;
import jpetstoretest.pageobjects.CatalogPageObject;
import jpetstoretest.pageobjects.CategoryPageObject;
import jpetstoretest.pageobjects.ItemPageObject;
import jpetstoretest.pageobjects.NewAccountPageObject;
import jpetstoretest.pageobjects.PageObjectBase;
import jpetstoretest.pageobjects.ProductPageObject;
import jpetstoretest.pageobjects.SignInPageObject;

public class StepDefinitions {

    @Before
    public void setup() {
        PageObjectBase.setDriver(new FirefoxDriver());
        PageObjectBase.setHost("http://localhost:8080");
    }

    @Given("I am logged in")
    public void i_am_logged_in() {
        final NewAccountPageObject newAccountPage = new NewAccountPageObject();
        final long timestamp = (new Timestamp(System.currentTimeMillis())).getTime();
        final String login = Long.toHexString(timestamp);
        final String password = login;
        final String email = login + "@example.com";
        newAccountPage.goTo();
        newAccountPage.fillNewAccountFields(login, password, email);
    }

    @When("I am on the New Account Page")
    public void displayNewAccountPage() {
        final NewAccountPageObject newAccountPage = new NewAccountPageObject();
        newAccountPage.goTo();
    }

    @When("I am on the Catalog Page")
    public void displayCatalogPage() {
        final CatalogPageObject catalogPage = new CatalogPageObject();
        catalogPage.goTo();
    }

    @When("I fill all New Account fields as {string} {string} {string} and click Save Account")
    public void fillNewAccountFields(final String login,
                                     final String password,
                                     final String email) {
        final NewAccountPageObject newAccountPage = new NewAccountPageObject();
        newAccountPage.assertIsCurrent();
        newAccountPage.fillNewAccountFields(login, password, email);
    }

    @When("I select the {string} category")
    public void i_select_the_category(final String category) {
        final CatalogPageObject catalogPage = new CatalogPageObject();
        catalogPage.assertIsCurrent();
        catalogPage.selectCategory(category);
    }

    @When("I select the {string} product")
    public void i_select_the_species(final String product) {
        final CategoryPageObject categoryPage = new CategoryPageObject();
        categoryPage.assertIsCurrent();
        categoryPage.selectProduct(product);
    }

    @When("I select the {string} item")
    public void i_select_the_product(final String item) {
        final ProductPageObject productPage = new ProductPageObject();
        productPage.assertIsCurrent();
        productPage.selectItem(item);
    }

    @When("I add the item to cart")
    public void i_add_the_species_to_cart() {
        final ItemPageObject itemPage = new ItemPageObject();
        itemPage.assertIsCurrent();
        itemPage.addToCart();
    }

    @Then("I can successfully sign in as {string} {string}")
    public void signIn(final String login,
                       final String password) {
        final SignInPageObject signInPage = new SignInPageObject();
        signInPage.goTo();
        signInPage.signIn(login, password);
    }

    @Then("the cart contains {string} {string} item")
    public void the_cart_contains_item(final String quantityAsText,
                                       final String item) {
        final CartPageObject cartPage = new CartPageObject();
        cartPage.goTo();
        final int expectedQuantity = Integer.parseInt(quantityAsText);
        final int effectiveQuantity = cartPage.getQuantityOfItem(item);
        Assertions.assertEquals(expectedQuantity, effectiveQuantity, "The expected quantity for item \"" + item + "\" is not the expected one");
    }

    @After()
    public void closeBrowser(final Scenario scenario) {
        if (scenario.isFailed()) {
        	final byte[] screenshot = PageObjectBase.getScreenshot();
            scenario.attach(screenshot, "image/png", "My screenshot");          
        }
        PageObjectBase.quit();
    }
}
