package jpetstoretest.pageobjects;

import org.openqa.selenium.By;

/**
 * Cart page
 *
 * @author lmazure
 */
public class CartPageObject extends PageObjectBase {
    
    public CartPageObject() {
        super("actions/Cart.action?viewCart=");
    }

    /**
     * Get the quantity of an item
     * 
     * @param item name of the item
     * @return quantity of the item
     */
    public int getQuantityOfItem(final String item) {
        // we need to normalize the text because multiword product name contains \r\n in the HTML source!
        final String quantityAsText = getDriver().findElement(By.xpath("//td[normalize-space(text())='" + item + "']/following-sibling::td[2]/input")).getAttribute("value");
        return Integer.parseInt(quantityAsText);
    }
}
