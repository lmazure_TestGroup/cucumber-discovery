package jpetstoretest.pageobjects;

import org.openqa.selenium.By;

/**
 * Catalog page
 * 
 * @author lmazure
 */
public class CatalogPageObject extends PageObjectBase {
    
    public CatalogPageObject() {
        super("actions/Catalog.action");
    }

    /**
     * Initiate the display of the category page (see {@link CategoryPageObject}) <code>category</code>
     *
     * @param category name of the category
     */
    public void selectCategory(final String category) {
        final String imageName = "sm_" + category.toLowerCase() + ".gif";
        getDriver().findElement(By.cssSelector("img[src*='" + imageName + "']")).click();
    }
}
