package jpetstoretest.pageobjects;

import org.openqa.selenium.By;

/**
 * Category page
 *
 * @author lmazure
 */
public class CategoryPageObject extends PageObjectBase {
    
    public CategoryPageObject() {
        //TODO add category in the constructor?
        super("actions/Catalog.action?viewCategory=");
    }

    /**
     * Initiate the display of the product page (see {@link ProductPageObject}) <code>product</code>
     *
     * @param product name of the product
     */
    public void selectProduct(final String product) {
        getDriver().findElement(By.xpath("//td[text()='" + product + "']/preceding-sibling::td[1]")).click();
    }
}
