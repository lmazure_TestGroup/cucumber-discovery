package jpetstoretest.pageobjects;

import org.openqa.selenium.By;

/**
 * Item page (an item is inside a product which is inside a category)
 * 
 * @author lmazure
 */
public class ItemPageObject extends PageObjectBase {
    
    public ItemPageObject() {
        //TODO add product in the constructor?
        super("actions/Catalog.action?viewItem=");
    }

    /**
     * Add the item to the cart
     */
    public void addToCart() {
        getDriver().findElement(By.xpath("//a[text()='Add to Cart']")).click();
    }
}
