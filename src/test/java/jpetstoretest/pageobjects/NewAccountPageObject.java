package jpetstoretest.pageobjects;

/**
 * Account creation page
 * 
 * @author lmazure
 */
public class NewAccountPageObject extends PageObjectBase {
    
    public NewAccountPageObject() {
        super("actions/Account.action?newAccountForm=");
    }

    /**
     * Fill the form with the specified values (default values are used for the other fields)
     * and initiate the account creation
     * 
     * @param userName
     * @param password
     * @param email
     */
    public void fillNewAccountFields(final String userName,
                                     final String password,
                                     final String email) {
        appendInNamedField("username", userName);
        appendInNamedField("password", password);
        appendInNamedField("repeatedPassword", password);
        appendInNamedField("account.firstName", "my first name");
        appendInNamedField("account.lastName", "my last name");
        appendInNamedField("account.email", email);
        appendInNamedField("account.phone", "my phone number");
        appendInNamedField("account.address1", "my address 1");
        appendInNamedField("account.address2", "my address 2");
        appendInNamedField("account.city", "my city");
        appendInNamedField("account.state", "my state");
        appendInNamedField("account.zip", "my zip");
        appendInNamedField("account.country", "my country");
        submitNewAccountForm();
        (new CatalogPageObject()).assertIsRedirectedTo();
    }

    /**
     * Initiate the account creation
     */
    private void submitNewAccountForm() {
        findElementByName("newAccount").click();
    }
}
