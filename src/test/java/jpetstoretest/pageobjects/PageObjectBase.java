package jpetstoretest.pageobjects;

import java.time.Duration;
import java.util.Objects;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Base class for all the page objects
 * 
 * @author lmazure
 */
public class PageObjectBase {
    private static WebDriver driver;
    private static String host;
    private final String url;

    protected PageObjectBase(final String url) {
        Objects.requireNonNull(host);
        Objects.requireNonNull(driver);
        this.url = url;
    }

    static public void setHost(final String host) {
        PageObjectBase.host = host;
    }

    static public void setDriver(final WebDriver driver) {
        PageObjectBase.driver = driver;
    }

    static public byte[] getScreenshot() {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

    static public void quit() {
        driver.quit();
    }

    public void goTo() {
        driver.get(getUrl());
    }

    public void assertIsRedirectedTo() {
        new WebDriverWait(getDriver(), Duration.ofSeconds(10)).until(ExpectedConditions.urlToBe(getUrl()));
    }
    
    public void assertIsCurrent() {
        final String effectiveUrl = driver.getCurrentUrl().replaceFirst("&.*$", "");
        Assertions.assertEquals(getUrl(), effectiveUrl, "Current URL is not the expected one");
    }

    protected WebDriver getDriver() {
        return driver;
    }

    protected String getUrl() {
        return host + "/" + url;
    }

    protected void appendInNamedField(final String fieldName,
                                      final String value) {
        final WebElement field = driver.findElement(By.name(fieldName));
        field.sendKeys(value);
    }

    protected void clearAndfillNamedField(final String fieldName,
                                          final String value) {
        final WebElement field = driver.findElement(By.name(fieldName));
        field.clear();
        field.sendKeys(value);
    }

    protected WebElement findElementByName(final String name) {
        return driver.findElement(By.name(name));
    }
}
