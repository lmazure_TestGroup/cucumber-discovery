package jpetstoretest.pageobjects;

import org.openqa.selenium.By;

/**
 * Product page (a product is inside a category)
 * 
 * @author lmazure
 */
public class ProductPageObject extends PageObjectBase {
    
    public ProductPageObject() {
        //TODO add product in the constructor
        super("actions/Catalog.action?viewProduct=");
    }

    /**
     * Initiate the display of the item page (see {@link ItemPageObject}) <code>item</code>
     *
     * @param item name of the item
     */
    public void selectItem(final String item) {
        // we need to normalize the text because multiword product name contains \r\n in the HTML source!
        getDriver().findElement(By.xpath("//td[normalize-space(text())='" + item + "']/preceding-sibling::td[2]")).click();
    }
}
