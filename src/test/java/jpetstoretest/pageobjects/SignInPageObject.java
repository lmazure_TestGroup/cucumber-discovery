package jpetstoretest.pageobjects;

/**
 * Login Page
 * 
 * @author lmazure
 */
public class SignInPageObject extends PageObjectBase {
    
    public SignInPageObject() {
        super("actions/Account.action?signonForm=");
    }

    /**
     * Initiate the login with the specified user name and password
     * 
     * @param userName
     * @param password
     */
    public void signIn(final String userName,
                       final String password) {
        appendInNamedField("username", userName);
        clearAndfillNamedField("password", password);
        submitSignInForm();
        (new CatalogPageObject()).assertIsRedirectedTo();
    }

    private void submitSignInForm() {
        findElementByName("signon").click();
    }
}
