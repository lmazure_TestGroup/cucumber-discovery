Feature: Account management
  Creating an account

  Scenario: Creating an account
    When I am on the New Account Page
    When I fill all New Account fields as "XXXmoi19" "XXXmonmotdepasse" "email4@example.com" and click Save Account
    Then I can successfully sign in as "XXXmoi19" "XXXmonmotdepasse"
