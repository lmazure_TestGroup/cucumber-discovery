Feature: Cart management
  Adding and removing things from the cart

  Scenario: Adding a pet in the cart
    Given I am logged in
    When I am on the Catalog Page
    And I select the "Fish" category
    And I select the "Koi" product
    And I select the "Spotless Koi" item
    And I add the item to cart
    Then the cart contains "1" "Spotless Koi" item
    
  Scenario: Adding two pets in the cart
    Given I am logged in
    When I am on the Catalog Page
    And I select the "Dogs" category
    And I select the "Golden Retriever" product
    And I select the "Adult Female Golden Retriever" item
    And I add the item to cart
    And I am on the Catalog Page
    And I select the "Reptiles" category
    And I select the "Iguana" product
    And I select the "Green Adult Iguana" item
    And I add the item to cart
    Then the cart contains "1" "Adult Female Golden Retriever" item
    And the cart contains "1" "Green Adult Iguana" item
